A = [-3 0 2; 5 8 -3; -2 0 1];
B = [1 -3; -4 2; 0 1];
l = [-1 ;-5 ;-1];
r = [3 ; 4];

display('Ex 0');
M0 = [(B),  (A*B) , (A*A*B)]
display('Ex 1');
M0_=M0(:,1:3);
determinant = det(M0_)

display('Ex 2');
c1 = -trace(A);
c2 = -(c1*trace(A)+trace(A*A))/2;
c3 = -(c2*trace(A)+c1*trace(A*A) + trace(A*A*A))/3;
c = [c3; c2; c1]

display('Ex 3');
c_= [-(l(1)*l(2)*l(3)); l(1)*l(2) + l(2)*l(3) + l(1)*l(3); -sum(l)]

display('Ex 4');
b=B*r;
d3 = b;
d2 = A*d3 + c(3)*b;
d1 = A*d2 + c(2)*b;

d = [d1, d2 , d3]
invd=inv(d)

display('Ex 5');
v=(c-c_)'*invd

display('Ex 6');
u=(r*v)
