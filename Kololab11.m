% A = [-1,8,-9,-5;3,3,-8,-3;6,-10,0,1;-9,8,9,0]
% B = [-9,-4;9,-2;-2,0;-2,-2];
% C = [0,0,0,1;0,0,1,0];
% P = [-8,-5,0,0];
% F = -2;
% A = [ -2 1 0 0; 0 -2 1 0; 0 0 -1 1; -1 0 0 0];
% B = [0 1; 0 0; 0 0; 1 0];
% C = [1 0 0 0; 0 0 1 0];
% P = [0 1 0 1];
% F = -3;

A =[5,5,-1,7;-4,-10,-5,3;-8,8,-1,-2;7,-7,4,4];
B = [0 -2; 8 -5; -10 -6; 0 4];
C = [1 0 0 0; 0 0 0 1];
P = [0 -10 2 0];
F = -3;

display('Ex 0');
[C' A'*C']
display('Ex 1');
det([C' A'*C'])
display('Ex 2');
syms t1 real t2 real
syms h1 real h2 real
for i = [1:4]
    ccc(i)=abs(sum(C(:,i))) > 0;
end
if     ((ccc(1) == 0) & (ccc(2) == 0))
    T = [P(1) P(2) t1 t2];
    H = [0 0 h1 h2];
elseif ((ccc(1) == 0) & (ccc(3) == 0))
    T = [P(1) t1 P(3) t2 ];
    H = [0 h1 0 h2 ];
elseif ((ccc(1) == 0) & (ccc(4) == 0))
    T = [P(1)  t1 t2 P(4)];
    H = [0  h1 h2 0];
elseif ((ccc(2) == 0) & (ccc(3) == 0))
    T = [t1 P(2) P(3) t2];
    H = [h1 0 0 h2];
elseif ((ccc(2) == 0) & (ccc(4) == 0))
    T = [t1 P(2) t2 P(4)];
    H = [ h1 0 h2 0];
elseif ((ccc(3) == 0) & (ccc(4) == 0))
    T = [t1 t2 P(3) P(4)];
    H = [h1 h2 0 0];
end
T

eqn = T*A - F*T == H;
[h1n, h2n, t1n, t2n] = solve(eqn);
Tn = vpa(subs (T,[t1 t2], [t1n t2n]),3)

display('Ex 3');
Hn = vpa(subs (H,[h1 h2], [h1n h2n]),3)

display('Ex 4');
vpa([F Hn Tn*B],3)

display('Ex 5');
vpa([Tn],3)
