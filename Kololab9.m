%A = [1 1 2; 3 -1 -1; -1 2 1];
%B = [1 2 1; -1 -1 2; 2 1 -1];
%l = [-2 1; -2 -1; -1 0];

A = [1 0 2; 3 -1 0; 2 3 1];
B = [1 2 4; -1 0 2; 2 1 3];
l = [-2 1; -2 -1; -1 0];
XiVi=[-2 0;0 -3;-4 0;-6 0;-0.2 0;0 -0.5;0 0.6;-4.5 0;-3 0]
compl = complex(l(:,1),l(:,2));

display('Ex 0');
q = [1 0 0]';
syms s real
Xs_arr = inv(s*eye(3) - A)*B*q;
for i = [1:3]
    [Xs(i), Us(i)] = numden(Xs_arr(i));
end
Xs = -Xs;
Us = -Us;

syms p1 real p2 real p3 real
eqn = p1*Xs(1) + p2*Xs(2) + p3*Xs(3) - ((s - compl(1))*(s - compl(2))*(s - compl(3)) - Us(2));
eqn1 = coeffs(eqn, s)';
[pn1,pn2,pn3] = solve(eqn1(1) == 0, eqn1(2) == 0,eqn1(3) == 0);

p = vpa([pn1 pn2 pn3],3)
P = [pn1 pn2 pn3; 0 0 0 ; 0 0 0]

display('Ex 1');
Ws = inv(s*eye(3) - A + B*P)*B;
[ws, ws_] = numden(Ws);

%Hotfix
ws(:,2)= ws(:,2)/9;
ws(:,3)= ws(:,3)/9;

syms m11 real m21 real m31 real
syms m12 real m22 real m32 real
syms m13 real m23 real m33 real

for n = [1:3]    
    m(1) = vpa(subs(ws(1,:)*[m11 m21 m31]', s, XiVi((n-1)*3 + 1, 1)), 3);
    m(2) = vpa(subs(ws(2,:)*[m11 m21 m31]', s, XiVi((n-1)*3 + 2, 1)), 3);
    m(3) = vpa(subs(ws(3,:)*[m11 m21 m31]', s, XiVi((n-1)*3 + 3, 1)), 3);
    
%     for j = [1:3]
%         if( XiVi((n-1)*3 + j, 2) > 0)
%             for i = [1:3]
%                 len = length(coeffs(ws(3,i))) ;
%                 if ( len > 2 )
%                     cws(i)=coeffs(ws(3, i));
%                 else
%                     cws(i) = 0;
%                 end
%             end
%             m(j) = vpa(cws*[m11 m21 m31]' - XiVi((n-1)*3 + j, 2),3);
%         end
%     end
    m'
    [m1n, m2n, m3n] = solve (m(1) == XiVi((n-1)*3 + 1, 2), m(2) == XiVi((n-1)*3 + 2, 2), m(3) == XiVi((n-1)*3 + 3, 2))
    M(:,n) = [m1n;m2n;m3n];
end
vpa(M,4)


display('Ex 2');
PP = inv(M)*P;
vpa(PP,4)
