
A = [2 8 -3; 3 6 -6; 2 3 1];
B = [-4; -5; 1];
l = [-1; -2 ;-2];

display('Ex 0');
M0 = [(B),  (A*B) , (A*A*B)]
display('Ex 1');
determinant = det(M0)

display('Ex 2');
c1 = -trace(A);
c2 = -(c1*trace(A)+trace(A*A))/2;
c3 = -(c2*trace(A)+c1*trace(A*A) + trace(A*A*A))/3;
c = [c3; c2; c1]

display('Ex 3');
c_= [-(l(1)*l(2)*l(3)); l(1)*l(2) + l(2)*l(3) + l(1)*l(3); -sum(l)]

display('Ex 4');
d3=B;
d2=A*B+c(3)*B;
d1=A*A*B + c(3)*A*B + c(2)*B;

d = [d1, d2 , d3]
invd=inv(d)

display('Ex 5');
Gt=(c-c_)'

display('Ex 6');
P=(Gt*invd)
