%A = [0 1 5; 2 5 0; 4 -3 0];
%B = [1 0; 0 1; 1 1];
%l = [-1; -2; -3];
%C = [0 2 0; 3 0 0];
A = [-2 -2 3; -3 5 2; 2 3 -2];
B = [-1 4; 2 -3; -2 -3];
l = [-1; -3; -3];
C = [1 0 4; 1 0 3];

display('Ex 0');
Su = [B A*B A*A*B]

display('Ex 1');
detSu = det(Su(1:3,1:3));
vpa(detSu)

display('Ex 2');
syms  q1 real q2 real s real;

q = [q1; q2];
SI = [s 0 0; 0 s 0; 0 0 s];

Ws = inv(SI-A)*B*q;

[gs1,Fs1] = numden((Ws(1)));
[gs2,Fs2] = numden((Ws(2)));
[gs3,Fs3] = numden((Ws(3)));
gs = [(coeffs(gs1,s)) ; (coeffs(gs2,s));(coeffs(gs3,s))];
L = gs(1:3,1:3)

E=C';
for i = [1:3]
    ccc(i)=abs(sum(E(i,:))) > 0;
end

Eb=E;
En=[ 0 0];
if(ccc(1) == 0) 
    Eb(1,:) = [];
    Ib = [ 0 1 0; 0 0 1];
    In = [1 0 0];
    l323 = [1 0 0; 1 l(2) l(2)^2; 1 l(3) l(3)^2];
elseif (ccc(2) == 0) 
    Eb(2,:) = [];
    Ib = [ 1 0 0; 0 0 1];
    In = [0 1 0];
    l323 = [1 l(1) l(1)^2; 0 1 0; 1 l(3) l(3)^2];
else
    Eb(3,:) = [];
    Ib = [ 1 0 0; 0 1 0];
    In = [0 0 1];
    l323 = [1 l(1) l(1)^2; 1 l(2) l(2)^2; 0 0 1];
end
Eb
In
Ib
alpha = (In - En*inv(Eb)*Ib)*inv(L');

syms S real
ag=coeffs((S-l(1))*(S-l(2))*(S-l(3)));
ag=ag(1:3);
Fs=coeffs(Fs2);
Fs=Fs(1:3);
eqn = alpha*ag' == alpha*Fs';
simplify(solve(subs(eqn, q2,1)))

qn1 = vpa(solve(subs(eqn, q2,1)))
qn2 = 1

display('Ex 3');

qn = [qn1; qn2];

Ln = subs(L, [q1 q2], [qn1(2)' 1]);

p = inv(Eb)*Ib*inv(Ln')*(ag' - Fs')


P = [qn1(2); 1]*p'

