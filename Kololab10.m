% A = [-2 1 0; 0 -1 1; 0 0 -3];
% B = [0; 0; 1 ];
% l = [-4; -4; -4];
% C = [1 0 0];

A =[ 3 -6 0; -9 -10 -7; -5 -2 8];
B = [0; 0; 1];
l = [-4; -5; -8];
C = [1 0 0];

display('Ex 0');
[C'  A'*C'  A'*A'*C']
display('Ex 1');
vpa(det([C'  A'*C'  A'*A'*C']))
display('Ex 2');
syms s real
coefs = coeffs((s - l(1))*(s - l(2))*(s - l(3)));
coefs(1:3)'
display('Ex 3');
syms k1 real k2 real k3 real
K = [k1;k2;k3];
t=det(s*eye(3) - A+K*C);
cc=coeffs(t,s);
[k1n, k2n, k3n] = solve(cc(1) == coefs(1), cc(2) == coefs(2),cc(3) == coefs(3));
vpa([k1n, k2n, k3n]',3)

display('Ex 4');
vpa(subs(A-K*C,[k1, k2, k3], [k1n, k2n, k3n]),3)