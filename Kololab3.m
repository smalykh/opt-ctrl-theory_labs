A = [-2 -3 2; -2 2 -1; 1 1 0];
B = [-2; -3; 2];
l = [-2; -3; -1];


display('Ex 0');
Su = [B A*B A*A*B]


display('Ex 1');
detSu = det(Su)


display('Ex 2');
c1 = -trace(A);
c2 = -(c1*trace(A)+trace(A*A))/2;
c3 = -(c2*trace(A)+c1*trace(A*A) + trace(A*A*A))/3;
c = [c3; c2; c1]


display('Ex 3');
c_= [-(l(1)*l(2)*l(3)); l(1)*l(2) + l(2)*l(3) + l(1)*l(3); -sum(l)]


display('Ex 4');
s = tf([1 0], [1]);
SI = [s 0 0; 0 s 0; 0 0 s];
Ws = inv(SI-A)*B;
minreal((Ws))


[gs1,Fs1] = tfdata(minreal(Ws(1)),'v');
[gs2,Fs2] = tfdata(minreal(Ws(2)),'v');
[gs3,Fs3] = tfdata(minreal(Ws(3)),'v');
gs = [gs1 ; gs2;  gs3]

Fs = Fs2(2:4);
c__ = [c_(3) c_(2) c_(1)];

AA = zeros(3,4);
AA = (gs(:, 2:4))';
tmp = (c__) - Fs;
linsolve(AA,tmp')
